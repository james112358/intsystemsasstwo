/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battle;

import static battle.Constants.SECONDS;
import static battle.Constants.TIME_LIMIT;
import battle.saucers.Saucer;
import battle.saucers.controllers.SaucerController;
import battle.saucers.controllers.SimpleController;
import battle.starfield.Starfield;

/**
 * Runs a game 
 * @author James
 */
public class QuickGame {
    
    
        //Adds th copntrollers to the array
        private static SaucerController[] controllers;
        static {
            try {
                controllers = new SaucerController[]{
                    new SimpleController(),
                    new SimpleController(),
                    new SimpleController(),
                    new SimpleController(),
                    new SimpleController(),
                    new SimpleController(),
                    new SimpleController(),
                    new SimpleController(),
                    new SimpleController(),
                    new SimpleController()
                };
            } catch (Exception e) {
                System.err.println("Unable to create controllers");
                e.printStackTrace(System.err);
                System.exit(0);
            }
        }

    
        public static void main(String[] args) throws Exception{
        
            // Field
            Starfield field= new Starfield(Constants.STARFIELD_WIDTH, Constants.STARFIELD_HEIGHT,Constants.STARFIELD_N_POWERUPS);
            
            //Time 
            double time = 0;
            //Add Controllers
            for (SaucerController controller : controllers) {
                field.addSaucer(new Saucer(field, controller));
            }
              
            
            while(field.getSaucerManager().numberAlive() > 1  && time < TIME_LIMIT)
            {
                    time += SECONDS;             
                    field.update(SECONDS);
            }
            
            field.finalisePlacings();
            
            System.out.println("Total time: " + time);
            System.out.println();
            field.getSaucers().forEach((saucer) -> {
                System.out.println(saucer.getControllerName()+"\t\t"+saucer.getPlacing());
            });
            System.out.println();
        
        }
    
    
}
